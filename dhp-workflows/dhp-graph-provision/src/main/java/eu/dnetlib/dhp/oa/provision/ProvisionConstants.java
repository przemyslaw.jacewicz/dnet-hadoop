
package eu.dnetlib.dhp.oa.provision;

public class ProvisionConstants {

	public static final int MAX_EXTERNAL_ENTITIES = 50;
	public static final int MAX_AUTHORS = 200;
	public static final int MAX_AUTHOR_FULLNAME_LENGTH = 1000;
	public static final int MAX_TITLE_LENGTH = 5000;
	public static final int MAX_TITLES = 10;
	public static final int MAX_ABSTRACT_LENGTH = 100000;
	public static final int MAX_INSTANCES = 10;

}
